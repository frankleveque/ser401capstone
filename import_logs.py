import json
import paramiko
from paramiko import SSHClient
from scp import SCPClient
import os
import re
import time

# This function requires that a public and private key pair (without a password) be established and
# shared with target IPs to allow for ssh and scp without a password. Also, a target user must be
# established on the target IPs.

def import_log():

    global configs
    current_time = int(time.time())

    # Define regex pattern for logs
    log_regex = re.compile('(scale_tests_)[A-Za-z0-9-]*(.log)')

    # Load log importer configurations
    with open('log_import_config.json', 'r') as config_options:
        configs = json.load(config_options)

    # Instantiate SSH client
    ssh = SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)
    ssh.load_system_host_keys()

    # Iterate through log types and configure import settings
    for log_types in configs.keys():
        ip_list = configs[log_types]['ipList']
        port = configs[log_types]['port']
        user = configs[log_types]['user']
        log_file_directory = configs[log_types]['logDirectory']

        print(log_types)

        # Locate and copy new log files from each testbed
        for ip in ip_list:

            try:
                # Connect to testbed through SSH
                ssh.connect(ip, port, user)

                print('Connected to ' + ip + ':')

                # Create string for destination directory
                log_type_directory = os.getcwd() + '/' + log_types + '/'

                # Navigate to log directory to get list of logs
                stdin, stdout, stderr = ssh.exec_command('cd '+ log_file_directory + '; ls; pwd')
                log_list = stdout.readlines()
                print('Found:\n' + "".join(log_list))

                # Instantiate SCPClient
                scp_client = SCPClient(ssh.get_transport())

                # Iterate through log directories and copy relevant logs. Skipped final item as it's the directory
                for log in log_list[:-1]:

                    # Search log for regex match. If found copy file to local directory
                    match = log_regex.search(log)
                    if match is not None:
                        stdin, stdout, stderr = ssh.exec_command("stat -c \'%Y\' " + log_file_directory + log.rstrip())
                        file_modified = stdout.readlines()

                        # Determine when log was added
                        time_elapsed = current_time - int(file_modified[0])

                        # Copy log if added since the last import (< 2hrs)
                        if time_elapsed < 7200:
                            scp_client.get(log_file_directory + log.rstrip(), log_type_directory + log.rstrip())
                            print('Imported ' + log.rstrip())

                scp_client.close()

                # Disconnect from host
                ssh.close()
                
            except (paramiko.BadHostKeyException, paramiko.AuthenticationException, paramiko.SSHException):
                print('Error occurred. Unable to import files')

if __name__ == "__main__":
    import_log()