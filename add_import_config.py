import json

if __name__ == "__main__":
    print("5HV " + ("-" * 30))
    five_ip_list = input("Enter the list of IP addresses (separated by spaces) where 5HV log files reside: ")
    five_log_directory = input("Enter the absolute URL to the folder that contains the 5HV log files: ")
    five_user = input("Enter the username that will be used to log in: ")
    five_port = input("Enter the port number the machine(s) will be accessed on: ")

    five_ip_array = five_ip_list.split()
    five_port = int(five_port)

    print("50HV " + ("-" * 30))
    fifty_ip_list = input("Enter the list of IP addresses (separated by spaces) where 50HV log files reside: ")
    fifty_log_directory = input("Enter the absolute URL to the folder that contains the 50HV log files: ")
    fifty_user = input("Enter the username that will be used to log in: ")
    fifty_port = input("Enter the port number the machine(s) will be accessed on: ")

    fifty_ip_array = fifty_ip_list.split()
    fifty_port = int(fifty_port)

    print("500HV " + ("-" * 30))
    five_hundred_ip_list = input("Enter the list of IP addresses (separated by spaces) where 500HV log files reside: ")
    five_hundred_log_directory = input("Enter the absolute URL to the folder that contains the 500HV log files: ")
    five_hundred_user = input("Enter the username that will be used to log in: ")
    five_hundred_port = input("Enter the port number the machine(s) will be accessed on: ")

    five_hundred_ip_array = five_hundred_ip_list.split()
    five_hundred_port = int(five_hundred_port)

    email_json = {
        "5HV": {
            "ipList": five_ip_array,
            "logDirectory": five_log_directory,
            "user": five_user,
            "port": five_port
        },
        "50HV": {
            "ipList": fifty_ip_array,
            "logDirectory": fifty_log_directory,
            "user": fifty_user,
            "port": fifty_port
        },
        "500HV": {
            "ipList": five_hundred_ip_array,
            "logDirectory": five_hundred_log_directory,
            "user": five_hundred_user,
            "port": five_hundred_port
        }
    }

    with open("log_import_config.json", "w") as outfile:
        json.dump(email_json, outfile, indent=4)
