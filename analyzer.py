from sklearn.cluster import DBSCAN
import sqlite3

import numpy
import sys
import os

db_path = os.path.join(os.path.dirname(__file__), "data.db")
conn = sqlite3.connect(db_path)

# collection of Machine Learning Classifiers, one for each test
classifier_dict = {}  # eg  {id: 1, MlpClassifier()}

def analyze_and_train():
    result = ""

    # get all test names
    test_cursor = conn.execute('''
        SELECT testname from TESTNAMES;''')

    # for each test in db
    for test_row in test_cursor:

        # insert test name into new cursor and get all build times for this test
        build_time_cursor = conn.execute('''
            SELECT DISTINCT build_time, test_id, filename
            FROM FILE_TESTS, Testnames, Filenames
            WHERE FILE_TESTS.file_id = Filenames.id and
            FILE_TESTS.test_id = Testnames.id and
            testname = ? ORDER BY build_time ASC
            LIMIT 500
            ''', (test_row[0],))

        test_id = None
        test_name = test_row[0]
        build_times = []
        file_names = []
        epsilon = None

        for build_row in build_time_cursor:
            build_times += [[build_row[0]]]
            test_id = build_row[1]
            file_names += [build_row[2]]

        assert len(build_times) == len(file_names)

        if len(build_times) > 1:
            mindiff = float(sys.maxsize)
            maxdiff = -1.0 * float(sys.maxsize)
            local_diff = None
            for i in range(len(build_times)-1):
                try:
                    assert(build_times[i+1][0] >= build_times[i][0])
                    local_diff = build_times[i+1][0] - build_times[i][0]
                    assert(local_diff >= 0)
                    mindiff = numpy.min([local_diff, mindiff])
                    maxdiff = numpy.max([local_diff, maxdiff])
                    assert(mindiff >= 0)
                    assert(maxdiff >= 0)
                except AssertionError:
                    print("ERROR ID", test_id)
                    if build_times[i+1][0] < build_times[i][0]:
                        print(build_times[i+1][0], " > ", build_times[i][0])
                    print("localdiff", local_diff)
                    print("max", maxdiff)
                    print("min", mindiff)
                    print(build_times)

            epsilon = numpy.max([numpy.mean([maxdiff, mindiff]), .1])

            # create classifier for this test
            classifier = DBSCAN(eps=epsilon, min_samples=3, n_jobs=-1)
            test_predictions = classifier.fit_predict(build_times)

            for i in range(len(test_predictions)):
                if test_predictions[i] == -1:
                    result += "(" + test_name + ") " + "Anomaly " + \
                              str(build_times[i][0]) + " in file " + file_names[i] + "\n"
        result += "\n"
    return result


if __name__ == "__main__":
    # run standalone
    conn = sqlite3.connect('data.db')
    print(analyze_and_train(), end="")
else:
    # from analyzer.py import analyze_and_train
    # then use analyze_and_train() as string
    pass
