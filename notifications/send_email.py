import smtplib
import json
import os
from email.message import EmailMessage

config_path = os.path.join(os.path.dirname(__file__), "notification_config.json")

def send_email(results):

    with open(config_path, "r") as config_file:
        config_options = json.load(config_file)

        # Creates email object and assigns header fields
        email = EmailMessage()
        email['Sender'] = config_options['sender']
        email['To'] = config_options['recipients']
        email['Subject'] = config_options['subject']

        # Assigns content to body of email
        if results == None:
            results = "No results returned"
        elif results == "":
            results = "No anomalies found"

        email.set_content(results)

        # Sends notification email
        email_server = smtplib.SMTP(config_options['server_ip'], config_options['server_port'])
        email_server.send_message(email)
        email_server.quit()

    return


def main():
    send_email("No results returned")


if __name__ == "__main__":
    main()










