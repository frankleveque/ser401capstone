import asyncore
from smtpd import DebuggingServer

#Runs debugging server to verify email configurations
def run_test_server():
    test_server = DebuggingServer(('127.0.0.1', 1025), None)
    asyncore.loop()

def main():
    run_test_server()

if __name__ == "__main__":
    main()