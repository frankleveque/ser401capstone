
#This file is to aid in creation and testing of json email
#config file. It is not meant for production and will be
#removed after the json config editor is implemented
import re
import json
def create_json_file():
    with open("notification_config.json", "w") as json_config_file:

        #Sending user
        from_user = ''

        #Receiving users
        to_users = list()

        #Subject line
        subject = 'Subject Line: Notification Test'

        #Body Content
        body_content = 'Body Content: Notification Test'

        #SMTP server IP and port
        smtp_server_ip = '127.0.0.1'
        smtp_server_port = 1025

        #Object to be serialized to JSON
        json_dict = {'sender':from_user, 'recipients': to_users, 'subject': subject, 'content': body_content,
                     'server_ip':smtp_server_ip, 'server_port': smtp_server_port}

        #Write json_dict to file
        json.dump(json_dict, json_config_file, indent=4)
        json_config_file.close

    return

# This will update the sender's email
def updateFromUser(usrEmail):
    with open('notification_config.json', 'r+') as j:
        data = json.load(j)

        if isValidEmail(usrEmail):
            data['sender'] = usrEmail  # <--- add `id` value.
            j.seek(0)  # <--- should reset file position to the beginning.
            json.dump(data, j, indent=4)
            j.truncate()  # remove remaining part
            j.close()
        else:
            tryAgain=input("Please enter a valid e-mail: ")
            j.close()
            updateFromUser(tryAgain)

# This will add a user to the distribution list
def addToUser(usrEmail):
    with open('notification_config.json', 'r+') as j:
        data = json.load(j)

        if isValidEmail(usrEmail):
            for x in range(0, len(data['recipients'])):
                if usrEmail.lower()== data['recipients'][x]:
                    print("This e-mail is already in the distribution list")
                    j.close
                    return
            data['recipients'].append(usrEmail) # <--- add `id` value.
            j.seek(0)  # <--- should reset file position to the beginning.
            json.dump(data, j, indent=4)
            j.truncate()  # remove remaining part
            j.close()
        else:
            tryAgain=input("Please enter a valid e-mail: ")
            j.close()
            addToUser(tryAgain)

#remove all Users From Distribution list
def clearUsers():
    with open('notification_config.json', 'r+') as j:
        data = json.load(j)
        del data['recipients'][:]  # <--- add `id` value.
        j.seek(0)  # <--- should reset file position to the beginning.
        json.dump(data, j, indent=4)
        j.truncate()  # remove remaining part
        j.close()

def removeToUser(usrEmail):
    with open('notification_config.json', 'r+') as j:
        data = json.load(j)

        if isValidEmail(usrEmail):
            if usrEmail in data['recipients']:
                data['recipients'].remove(usrEmail)
                j.seek(0)  # <--- should reset file position to the beginning.
                json.dump(data, j, indent=4)
                j.truncate()  # remove remaining part
                j.close()
            else:
                print("Recipient not found")
        else:
            tryAgain=input("Please enter a valid e-mail: ")
            j.close()
            removeToUser(tryAgain)

# This checks to make sure the e-mail is valid
def isValidEmail(email):
    if len(email) > 7:
        if re.match("^.+@([?)[a-zA-Z0-9-.]+.([a-zA-Z]{2,3}|[0-9]{1,3})(]?)$)", email) != None:
            return True
    return False

#add content of the message
def addContent(message):
    with open('notification_config.json', 'r+') as j:
        data = json.load(j)
        data['content'] = message  # <--- add `id` value.
        j.seek(0)  # <--- should reset file position to the beginning.
        json.dump(data, j, indent=4)
        j.truncate()  # remove remaining part
        j.close()
#
def addSubject(subject):
    with open('notification_config.json', 'r+') as j:
        data = json.load(j)
        data['subject'] = subject  # <--- add `id` value.
        j.seek(0)  # <--- should reset file position to the beginning.
        json.dump(data, j, indent=4)
        j.truncate()  # remove remaining part
        j.close()

def updateServerIP(server_ip):
    with open('notification_config.json', 'r+') as j:
        data = json.load(j)
        data['server_ip'] = server_ip  # <--- add `id` value.
        j.seek(0)  # <--- should reset file position to the beginning.
        json.dump(data, j, indent=4)
        j.truncate()  # remove remaining part
        j.close()

def updateServerPort(server_port):
    with open('notification_config.json', 'r+') as j:
        try:
            server_p = int(server_port)
            data = json.load(j)
            data['server_port'] = server_p # <--- add `id` value.
            j.seek(0)  # <--- should reset file position to the beginning.
            json.dump(data, j, indent=4)
            j.truncate()  # remove remaining part
            j.close()
        except:
            print('Invalid port number')

