import json
from notifications import CreatUpdateJson

# This module provides a CLI for updating the notifications configuration file.

# Displays current notification configurations and main menu.
def display_config_menu():
    with open("notification_config.json", "r") as config_file:
        config_options = json.load(config_file)

    print("\nCurrent configurations:")

    for option_type, option in config_options.items():
        print(str(option_type) + ': ' + str(option))

    print("\nEnter an option number to update configuration notifications:\n")

    print('1) Update Sender\n'
          '2) Add Recipients\n'
          '3) Remove Recipients\n'
          '4) Update Subject\n'
          '5) Update Server\n'
          '6) Update Server Port\n'
          '7) Quit\n')

def main():

    config_complete = False

    display_config_menu()

    # Allows user to make menu selections and configuration changes until option 7 is selected to exit.
    while not config_complete:

        user_input = input('Selection: ')

        if user_input == '1':
            correct = False
            while not correct:
                new_sender = input('Enter new sender email address: ')
                confirm = input('Is this correct? ' + new_sender + '\n (y/n): ')

                if confirm == 'y':
                    CreatUpdateJson.updateFromUser(new_sender)
                    correct = True
                else:
                    pass

            display_config_menu()

        elif user_input == '2':
            correct = False
            while not correct:
                new_recipient = input('Enter new recipient email address: ')
                confirm = input('Is this correct? ' + new_recipient + '\n (y/n): ')

                if confirm == 'y':
                    CreatUpdateJson.addToUser(new_recipient)
                    correct = True
                else:
                    pass

            display_config_menu()

        elif user_input == '3':
            correct = False
            while not correct:
                remove_recipient = input('Enter email address of recipient to remove: ')
                confirm = input('Is this correct? ' + remove_recipient + '\n (y/n): ')

                if confirm == 'y':
                    CreatUpdateJson.removeToUser(remove_recipient)
                    correct = True
                else:
                    pass

            display_config_menu()

        elif user_input == '4':
            correct = False
            while not correct:
                subject_line = input('Enter new subject line: ')
                confirm = input('Is this correct? ' + subject_line + '\n (y/n): ')

                if confirm == 'y':
                    CreatUpdateJson.addSubject(subject_line)
                    correct = True
                else:
                    pass

            display_config_menu()

        elif user_input == '5':
            correct = False
            while not correct:
                server_IP = input('Enter new server IP address: ')
                confirm = input('Is this correct? ' + server_IP + '\n (y/n): ')

                if confirm == 'y':
                    CreatUpdateJson.updateServerIP(server_IP)
                    correct = True
                else:
                    pass

            display_config_menu()

        elif user_input == '6':
            correct = False
            while not correct:
                server_port = input('Enter new server port: ')
                confirm = input('Is this correct? ' + server_port + '\n (y/n): ')

                if confirm == 'y':
                    CreatUpdateJson.updateServerPort(server_port)
                    correct = True
                else:
                    pass

            display_config_menu()

        elif user_input == '7':
            config_complete = True

        else:
            print("Invalid Option")

if __name__ == '__main__':
    main()