import sqlite3
conn = sqlite3.connect('data.db')

def create_table():
    try:

        conn.execute('''
        CREATE TABLE Filenames(
          id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
          filename TEXT UNIQUE); ''');

        conn.execute('''
        CREATE TABLE Testnames(
          id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
          testname TEXT UNIQUE); ''');

        conn.execute('''
        CREATE TABLE FILE_TESTS(
          file_id INTEGER NOT NULL, 
          test_id INTEGER NOT NULL,
          build_time REAL,
          foreign key (file_id) references Filenames(id) ON DELETE CASCADE,
          foreign key (test_id) references Testnames(id) ON DELETE CASCADE,
          CONSTRAINT uniq0 UNIQUE(file_id, test_id, build_time)
          ); ''');

        conn.execute('''
        CREATE TABLE DISRUPTION_TESTS(
          file_id INTEGER NOT NULL, 
          batch_time REAL,
          foreign key (file_id) references Filenames(id) ON DELETE CASCADE,
          CONSTRAINT uniq1 UNIQUE(file_id, batch_time)
          ); ''');

    except Exception as e:
        print("Exception caught: " + str(e))

def main():
    create_table()

if __name__ == "__main__":
    main()

