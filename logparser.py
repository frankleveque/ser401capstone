#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import os
import datetime
import time
import glob
import shutil
import sqlite3
from disruption_parser import parse_disrupt

conn = None

def parse(filename, foldertype):
    result = ""
    start = None
    end = None
    failedtest = False
    started = False
    lineNumber = 0
    with open(filename, 'r') as log:
        last_run_test = None
        line = log.readline()
        while line != '':
            lineNumber += 1
            if "::scale_testcase[" in line or "API call to get all" in line:
                first_space = line.find(" ")
                rest = line[first_space+1:].split("::")
                assert(len(rest) == 6)
                date = line[0 : first_space]
                timestamp = rest[0]
                thread = rest[1]
                testnum = rest[2]
                testgroup = rest[3]
                testtype = rest[4]
                testmessage = rest[5]

                timestamp = timestamp.split(',')
                timestamp[1] = timestamp[1]+'000'  # converts to microseconds for datetime
                timestamp = ":".join(timestamp)

                fulldate = date + " " + timestamp
                fulldatestamp = datetime.datetime.strptime(fulldate, "%Y-%m-%d %H:%M:%S:%f")
                diff = None

                if("running" in testmessage.lower()):
                    try:
                        assert(not started)
                        start = fulldatestamp
                        last_run_test = testmessage

                    except AssertionError:
                        result += "Could not find test ending\n\n"
                        print("\tTest end not found: %s" % last_run_test.split("\n")[0])
                        last_run_test = testmessage

                    result += testmessage.rstrip()

                    # Added line number to output for debugging purposes, comment out if not required and remove .rstrip() from result
                    result += " ***Line: " + str(lineNumber) + "***\n"

                    started = True

                    # For "Configure Distributed Firewall Test", send to disruption_parser
                    if "Configure Distributed Firewall Test" in testmessage:
                        print("Attempting Disruption Parsing at line " + str(lineNumber))
                        conn.commit()
                        lineNumber = parse_disrupt(filename, log, lineNumber, conn)
                        conn.commit()

                elif "minimum" in testmessage.lower() or " ERROR " in line or "Failure" in line or "API call" in line:
                    if ("minimum" in testmessage.lower() and "fail" in testmessage.lower()) or "uuid" in testmessage.lower():
                        failedtest = True
                        result, started = recordTestEnd(filename, fulldatestamp, last_run_test, lineNumber, result, start, started, testmessage, foldertype)
                    elif failedtest:
                        failedtest = False
                    else:
                        result, started = recordTestEnd(filename, fulldatestamp, last_run_test, lineNumber, result, start, started, testmessage, foldertype)

            line = log.readline()
    return result


def recordTestEnd(filename, fulldatestamp, last_run_test, lineNumber, result, start, started, testmessage, foldertype):
    result += testmessage.rstrip()
    # Added line number to output for debugging purposes, comment out if not required and remove .rstrip() from result
    result += " ***Line: " + str(lineNumber) + "***\n"
    try:
        assert (started)
        end = fulldatestamp
        diff = end - start
        result += ("Took " + str(diff.total_seconds()) + " seconds")
        testname = " ".join((last_run_test.split("\n")[0]).split(" ")[4:])  # get just the test name
        testname += " " + foldertype
        try:
            conn.execute('''
                            INSERT INTO Filenames
                            values(NULL, "%s"); ''' % filename)
        except sqlite3.IntegrityError:
            pass  # unique constraint failed (already in db) - continue

        try:
            conn.execute('''
                            INSERT INTO Testnames
                            values(NULL, "%s"); ''' % testname)
        except sqlite3.IntegrityError:
            pass  # unique constraint failed (already in db) - continue

        try:
            conn.execute('''
                            REPLACE INTO File_Tests
                            values(
                                (SELECT id FROM Filenames where filename = "%s"),
                                (SELECT id FROM Testnames where testname = "%s"),
                                %f);''' % (filename, testname, diff.total_seconds()))
        except Exception as e:
            print(e)

    except AssertionError:
        result += "Could not find test beginning"
        print("\tTest start not found: %s" % testmessage.split("\n")[0])
    result += '\n\n'
    started = False
    return result, started


if __name__ == "__main__":
    conn = sqlite3.connect('data.db')
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    print('Starting parser at ' + os.getcwd())
    log_folders = ['5HV', '50HV', '500HV']
    #resfolder = 'results'

    # remove results folder before run
    #if os.path.isdir(resfolder):
    #    shutil.rmtree(resfolder, ignore_errors=True)

    for log_folder in log_folders:
        # check if log folder exists, make if needed
        if not os.path.isdir(log_folder):
            os.mkdir(log_folder)

        log_files = []
        log_files += glob.glob(log_folder + '/*.log')
        for log_file in log_files:
            print(log_file)
            parse(log_file, log_folder)

    conn.commit()

