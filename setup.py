from setuptools import setup, find_packages

setup(
    name='ser401capstone',
    version='1.0',
    packages=find_packages(),
    url='https://bitbucket.org/frankleveque/ser401capstone',
    license='',
    author='SER 401/402 Team 13',
    author_email='scarnead@asu.edu',
    description='Analyzes build logs for timing anomalies',
    install_requires=[
        'numpy',
        'scikit-learn',
        'paramiko',
        'scp',
        'scipy'
    ]
)
