import datetime

# Takes over parsing for Configure Distributed Firewall Test

# Extract timestamp and date from line input
def time_parse(line):
    first_space = line.find(" ")
    rest = line[first_space + 1:].split("::")
    assert (len(rest) == 6)
    date = line[0: first_space]
    timestamp = rest[0]
    return date, timestamp

# Convert date and timestamp to datetime
def datetime_conversion(date, timestamp):
    timestamp = timestamp.split(',')
    timestamp[1] = timestamp[1] + '000'
    timestamp = ":".join(timestamp)

    fulldate = date + " " + timestamp
    fulldatestamp = datetime.datetime.strptime(fulldate, "%Y-%m-%d %H:%M:%S:%f")
    return fulldatestamp

# Inserts successful batch times into DISRUPTION_TESTS db table
def insert_record(filename, batch_time, dbconn):
    try:
        dbconn.execute('''
        REPLACE INTO DISRUPTION_TESTS
        values(
            (SELECT id FROM Filenames where filename = "%s"),
            %f);''' % (filename, batch_time))

    except Exception as e:
        print(e)


# Main disruption parsing function
def parse_disrupt(filename, openfile, lineNum, dbconn):
    print('Running Disruption Parser')
    disrupt_strings = ["Batch number", "Minimum"]  # Keywords for start and stop points
    end_test_case_flags = ["Running Cycle"]
    result_string = ""
    line_number = lineNum
    file_position = openfile.tell()
    pass_back_position = openfile.tell()
    pass_back_line = line_number
    end_test_case = False
    min_found = False
    batch_number = 1

    while not end_test_case:

        line = openfile.readline()
        line_number += 1

        # If keywords are found in line, add line to result string
        if any(s in line for s in disrupt_strings):
            if "Batch number" in line:
                date, timestamp = time_parse(line)
                start = datetime_conversion(date, timestamp)
                result_string += "Line " + str(line_number) + " Batch # " + str(batch_number) + " starts at " + str(start) + "\n"
            elif "Minimum" in line:
                date, timestamp = time_parse(line)
                end = datetime_conversion(date, timestamp)
                batch_time = (end - start).total_seconds()
                insert_record(filename, batch_time, dbconn)
                result_string += "Line " + str(line_number) + " Batch # " + str(batch_number) + " ends at " + str(end) + "\n"
                result_string += "Batch # " + str(batch_number) + " took " + str(batch_time) + " seconds\n"
                batch_number += 1
                min_found = True
                pass_back_position = file_position
                pass_back_line = line_number - 1

        # Breaks loop when next test case is encountered
        if any(s in line for s in end_test_case_flags):
            if not min_found:
                pass_back_position = file_position
                pass_back_line = line_number - 1
            break

        # Ends loop if eof is reached
        if line == '' or 'ERROR' in line:
            pass_back_position = file_position
            pass_back_line = line_number - 1
            if not min_found:
                print("Batch # " + str(batch_number) + " failed")
            end_test_case = True

        file_position = openfile.tell()

    #Temporarily writing results to output file
    #with open("../results/" + filename.rstrip('.log') + "dp_output.txt", 'w') as res:
    #    res.write(result_string)

    openfile.seek(pass_back_position)  # Reset file position to line before next test case

    print("Resuming normal parsing at line " + str(pass_back_line))

    return pass_back_line  # Return line number of line before next test case


