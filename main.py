import init_sqlite
import logparser
import analyzer
import sqlite3
import import_logs
import os
from notifications import send_email
import glob
from pathlib import Path

if __name__ == "__main__":

    send_notification = True;

    # cd to this containing folder
    os.chdir(os.path.dirname(os.path.realpath(__file__)))

    # init DB if needed
    if not Path("data.db").exists():
        init_sqlite.conn = sqlite3.connect('data.db')
        init_sqlite.create_table()  # Initialize db
        init_sqlite.conn.commit()
        init_sqlite.conn.close()

    all_logfiles = []  # list of all new log files
    log_folders = ['5HV', '50HV', '500HV']  # folders that store log files

    # Check for new logs an import as required
    import_logs.import_log()


    # Analyze logs in log folder
    print('\n' + "-" * 90)
    print('\nParsing...\n')
    logparser.conn = sqlite3.connect('data.db')
    for log_folder in log_folders:
        # check if log folder exists, make if needed
        if not os.path.isdir(log_folder):
            os.mkdir(log_folder)

        log_files = []
        log_files += glob.glob(log_folder + '/*.log')  # get all logfiles in current logfolder
        all_logfiles += log_files  # add them to large array for later

        if not all_logfiles:
            send_notification = False

        for log_file in log_files:  # parse each logfile
            print(log_file)
            logparser.parse(log_file, log_folder)

    logparser.conn.commit()
    logparser.conn.close()

    # Run analzyer and send results if log files found
    if send_notification:
        print('\n' + "-" * 90)
        print("Analyzing...")
        analyzer.conn = sqlite3.connect('data.db')
        all_results = analyzer.analyze_and_train()
        anomalies = ""

        # print out anomalies only in files we're currently using
        for line in all_results.split("\n"):
            for log_file in all_logfiles:
                if log_file in line:
                    anomalies += line + "\n"

        analyzer.conn.commit()
        analyzer.conn.close()

        # Send anomalies via email if log files are detected
        try:
            print('\nFound the following anomalies:')
            print(anomalies)
            send_email.send_email(anomalies)
        except:
            print("Could not send results notification")
    else:
        print('No new log files found.')

    # Empty log file folders so that parser only runs new logs
    for folder in log_folders:
        os.chdir('./' + folder)
        print('Removing logs from ' + os.getcwd())
        logs = os.listdir()
        for log in logs:
            os.remove(os.getcwd() + '/' + log)
        os.chdir('../')
